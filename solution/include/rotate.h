#include <inttypes.h>
#include <malloc.h>

#include "../include/bmp.h"
#include "../include/utils.h"


void rotate_image(struct image* img, int32_t angle);
