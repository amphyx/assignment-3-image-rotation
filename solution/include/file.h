#include <stdbool.h>
#include <stdio.h>

#define FILE_OPEN_ERROR "Ошибка при чтении файла"

#define READ_MODE "rb"

#define WRITE_MODE "wb"

FILE* open_file(const char* filename, const char* mode);

bool close_file(const FILE* opened_file);
