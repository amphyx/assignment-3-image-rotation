#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


enum args_parsing_status {
    ARGS_PARSE_OK = 0,
    ARGS_BAD_COUNT,
    ARGS_PARSE_ANGLE_NAN,
    ARGS_PARSE_ANGLE_UNSUPPORTED,
};

extern const char* ARGS_PARSING_ERRORS[];

struct parsed_args {
    enum args_parsing_status parse_status;
    const char* source_filename;
    const char* transformed_filename;
    const int32_t angle;
};

struct parsed_args parse_args(int32_t argc, char** argv);
