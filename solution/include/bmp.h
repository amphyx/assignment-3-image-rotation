#pragma once

#include <inttypes.h>
#include <malloc.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>


/**
 * Заголовок BMP-файла.
*/
struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

/**
 * RGB-пиксель.
*/
struct __attribute__((packed)) pixel { uint8_t b, g, r; };

/**
 * Представление BMP-изображения: его размеры и набор всех пикселей.
*/
struct image {
    uint64_t width, height;
    struct pixel* data;
};

/**
 * Статус чтения файла.
*/
enum read_status {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_SIZE,
};

/**
 * Статус записи файла.
*/
enum write_status {
  WRITE_OK = 0,
  WRITE_ERROR
};

extern const char* READ_ERRORS[];

extern const char* WRITE_ERRORS[];

enum read_status from_bmp(FILE* in, struct image* img);

enum write_status to_bmp(FILE* out, const struct image* img);

uint64_t get_data_size(const struct image* img);

struct pixel get_pixel_on_position(const struct image* img, const size_t row, const size_t column);
