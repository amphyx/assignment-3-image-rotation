#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>

FILE* open_file(const char* filename, const char* mode) {
    if (!filename) {
        return NULL;
    }

    return fopen(filename, mode);
}

bool close_file(FILE* opened_file) {
    if (!opened_file) {
        return true;
    }
    int32_t result_status = fclose(opened_file);

    return result_status == 0 ? true : false;
}
