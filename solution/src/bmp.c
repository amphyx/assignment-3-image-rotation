#include "../include/file.h"
#include "../include/bmp.h"
#include "../include/utils.h"

/**
 * Правильная сигнатура BMP-файла.
 *
 * Представлен в виде uint16_t, так как в структуре поле заголовка в этом типе.
*/
#define VALID_BMP_SIGNATURE (('M' << 8) + 'B')

/**
 * Поддерживаемое количество битов на пиксель.
*/
#define SUPPORTED_BITS_PER_PIXEL 24

/**
 * Поддерживаемый тип сжатия.
*/
#define SUPPORTED_COMPRESSION 0

/**
 * Значение для заполнения зарезервированной части.
*/
#define RESERVED_VALUE 0

/**
 * Размер `InfoHeader` в байтах.
*/
#define INFO_HEADER_SIZE 40

/**
 * Количество слоёв.
*/
#define NUMBER_OF_PLANES 1

/**
 * Количество используемых цветов.
 *
 * Из спеков:
 * Bits per Pixel used to store palette entry information.
 * This also identifies in an indirect way the number of possible
 * colors. Possible values are:
 * 1 = monochrome palette. NumColors = 1
 * 4 = 4bit palletized. NumColors = 16
 * 8 = 8bit palletized. NumColors = 256
 * 16 = 16bit RGB. NumColors = 65536
 * 24 = 24bit RGB. NumColors = 16M
*/
#define COLORS_USED 16000000

/**
 * Число основных цветов.
 *
 * Из спеков:
 * 0 = all
*/
#define IMPORTANT_COLORS 0

/**
 * Чему должна быть кратна ширина BMP-файла.
*/
#define PADDING_FACTOR 4

const char* READ_ERRORS[] = {
    [READ_INVALID_SIGNATURE] = "Неверная сигнатура BMP-файла.",
    [READ_INVALID_BITS] = "Поддерживается только 24-битные BMP-файлы.",
    [READ_INVALID_HEADER] = "Заголовок BMP-файла повреждён.",
    [READ_INVALID_SIZE] = "Неправильный размер BMP-файла.",
};

const char* WRITE_ERRORS[] = {
    [WRITE_ERROR] = "Не удалось записать файл.",
};

/**
 * Проверить сигнатуру BMP-файла из его заголовков.
 *
 * @param header указатель на заголовок
 * @returns `true`, если сигнатура правильная
*/
static bool check_bmp_signature(struct bmp_header* header) {
  // Далее действия для перевода uint16_t в два char
  return header->bfType == VALID_BMP_SIGNATURE;
}

/**
 * Проверить количество битов на пиксель BMP-файла из его заголовков.
 *
 * @param header указатель на заголовок
 * @returns `true`, если количество поддерживается
*/
static bool check_bmp_bits(struct bmp_header* header) {
  return header->biBitCount == SUPPORTED_BITS_PER_PIXEL;
}

/**
 * Получить padding в байтах.
 *
 * @param img указатель на структуру изображения
*/
static uint64_t get_padding_bytes(const struct image* img) {
  return ceil_division_uint64(img->width * 3, PADDING_FACTOR) * PADDING_FACTOR - img->width * 3;
}

/**
 * Получить размер области с пикселями в байтах.
 *
 * @param img указатель на структуру изображения
*/
uint64_t get_data_size(const struct image* img) {
  const uint64_t total_pixels = img->width * img->height;
  return total_pixels * sizeof(struct pixel);
}

/**
 * Получить пиксель по его координатам строки и столбца.
 *
 * Важно: строка и столбец нумеруются с нуля!
 *
 * @param img указатель на структуру изображения
 * @param row номер строки (начинается с нуля)
 * @param column номер столбца (начинается с нуля)
 * @returns структура пикселя
*/
struct pixel get_pixel_on_position(const struct image* img, const size_t row, const size_t column) {
  const size_t single_dim_index = row * img->width + column;
  return img->data[single_dim_index];
}

/**
 * Получить размер изображения в байтах.
 *
 * <размер> = <заголовок> + <пиксели> + padding * <кол-во строк>
 *
 * @param img указатель на структуру изображения
 * @returns размер изображения в байтах
*/
static uint64_t get_file_size(const struct image* img) {
  const uint64_t total_padding = get_padding_bytes(img) * img->height;
  return sizeof(struct bmp_header) + get_data_size(img) + total_padding;
}

static struct bmp_header create_header(const struct image* img) {
  return (struct bmp_header) {
    .bfType = VALID_BMP_SIGNATURE,
    .bfileSize = get_file_size(img),
    .bfReserved = RESERVED_VALUE,
    .bOffBits = sizeof(struct bmp_header),
    .biSize = INFO_HEADER_SIZE,
    .biWidth = img->width,
    .biHeight = img->height,
    .biPlanes = NUMBER_OF_PLANES,
    .biBitCount = SUPPORTED_BITS_PER_PIXEL,
    .biCompression = SUPPORTED_COMPRESSION,
    .biSizeImage = 0,  // Из спеков: It is valid to set this =0 if Compression = 0
    .biXPelsPerMeter = 11811,  // 300 dpi
    .biYPelsPerMeter = 11811,  // 300 dpi
    .biClrUsed = COLORS_USED,
    .biClrImportant = IMPORTANT_COLORS,
  };
}

/**
 * Получить указатель на последнюю строку пикселей изображения.
 *
 * Это нужно, так как в BMP-файле пиксели хранятся в порядке от последней строки к первой.
 *
 * @param img указатель на структуру изображения
 * @returns указатель на последнюю строку
*/
static struct pixel* get_last_data_row(const struct image* img) {
  return img->data + img->width * img->height - img->width;
}

/**
 * Считать открытый файл `in` в структуру BMP-изображения `img`.
 * 
 * @param in указатель на открытый файл для чтения
 * @param img указатель на структуру, в которую помещаются считанные данные
 * @returns статус чтения файла с информацией о том, успешно ли или произошла конкретная ошибка
*/
enum read_status from_bmp(FILE* in, struct image* img) {
  struct bmp_header header = { 0 };

  const size_t read_objects = fread(&header, sizeof(struct bmp_header), 1, in);
  if (read_objects != 1) {
    return READ_INVALID_HEADER;
  }

  bool is_signature_valid = check_bmp_signature(&header);
  if (!is_signature_valid) {
    return READ_INVALID_SIGNATURE;
  }

  bool is_bits_valid = check_bmp_bits(&header);
  if (!is_bits_valid) {
    return READ_INVALID_BITS;
  }

  img->width = header.biWidth;
  img->height = header.biHeight;
  const uint64_t data_size = get_data_size(img);
  // Аллоцируем место в куче для пикселей
  img->data = malloc(data_size);

  // Получаем указатель на последний ряд пикселей, поскольку надо читать снизу вверх
  struct pixel* data_ptr = get_last_data_row(img);
  const uint64_t padding = get_padding_bytes(img);

  for(size_t row = 0; row < img->height; row++) {
    // Читаем пиксели до начала padding
    const size_t read_pixels = fread(data_ptr, sizeof(struct pixel), img->width, in);
    if (read_pixels != img->width) {
      return READ_INVALID_SIZE;
    }
    // Пропускаем байты padding
    fseek(in, (long) padding, SEEK_CUR);
    // Сдвигаем указатель на строку пикселей выше
    data_ptr -= img->width;
  }

  return READ_OK;
}

/**
 * Записать представление BMP-файлы из структуры `img` в открытый файл `out`.
 * 
 * @param out указатель на открытый файл для записи
 * @param img указатель на структуру, в которую помещаются считанные данные
 * @returns статус записи файла с информацией о том, успешно ли или произошла конкретная ошибка
*/
enum write_status to_bmp(FILE* out, const struct image* img) {
  const struct bmp_header header = create_header(img);
  size_t written_objects = fwrite(&header, sizeof(struct bmp_header), 1, out);
  if (written_objects != 1) {
    return WRITE_ERROR;
  }

  // Получаем указатель на последний ряд пикселей, поскольку надо записывать снизу вверх
  struct pixel* data_ptr = get_last_data_row(img);

  const uint64_t padding = get_padding_bytes(img);

  // Нолики для записи padding
  const uint8_t zeroes[4] = {0};

  for(size_t row = 0; row < img->height; row++) {
    // Пишем строку с пикселями
    const size_t written_pixels = fwrite(data_ptr, sizeof(struct pixel), img->width, out);
    if (written_pixels != img->width) {
      free(img->data);
      return WRITE_ERROR;
    }

    // Пишем оставшийся padding
    const size_t written_padding = fwrite(zeroes, padding, 1, out);
    if (written_padding != 1) {
      free(img->data);
      return WRITE_ERROR;
    }

    // Сдвигаем указатель на предыдущую строку пикселей
    data_ptr -= img->width;
  }

  free(img->data);  // Освобождаем аллоцированное место пикселей после записи
  return WRITE_OK;
}
