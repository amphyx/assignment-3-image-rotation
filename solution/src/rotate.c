#include "../include/rotate.h"

/**
 * Повернуть изображение на 90 градусов по часовой стрелке.
 *
 * @param img указатель на изображение
*/
static void rotate_90_degrees(struct image* img) {
    // Аллоцируем новый массив пикселей
    struct pixel* rotated_data = malloc(get_data_size(img));

    size_t i = 0;  // Одномерный индекс для текущего пикселя в rotated_data
    // Берём пиксели поочерёдно снизу вверх и слева направо, заполняя в таком порядке rotated_data
    for (size_t column = 0; column < img->width; ++column) {
        for (size_t row = img->height - 1; row > 0; --row) {
            rotated_data[i] = get_pixel_on_position(img, row, column);
            i++;
        }
        // Обрабатываем row = 0, поскольку так как size_t беззнаковый, то невозможно сделать for до нуля
        rotated_data[i] = get_pixel_on_position(img, 0, column);
        i++;
    }

    // Удаляем старые пиксели
    free(img->data);

    // Запоминаем новые пиксели
    img->data = rotated_data;
    // Обновляем размеры картинки: меняем ширину на высоту и наоборот
    const uint64_t temp = img->width;
    img->width = img->height;
    img->height = temp;
}

/**
 * Найти аналогичный положительный угол для отрицательного.
 *
 * Аналогичные углы:
 * -90 = 270
 * -180 = 180
 * -270 = 90
 *
 * @param angle отрицательный угол
 * @returns аналогичный угол
*/
static int32_t convert_negative_angle_to_positive(int32_t angle) {
    return angle + 360;
}


/**
 * Повернуть изображение на заданный угол.
 *
 * @param img указатель на изображение
 * @param angle угол, кратный 90 градусам
*/
void rotate_image(struct image* img, int32_t angle) {
    if (angle < 0) {
        angle = convert_negative_angle_to_positive(angle);
    }

    const uint8_t rotates_on_90_number = ceil_division(angle, 90);
    for (size_t i = 0; i < rotates_on_90_number; i++) {
        rotate_90_degrees(img);
    }
}

