#include "../include/args.h"


// Количество аргументов
#define ARGC 4

const char* ARGS_PARSING_ERRORS[] = {
    [ARGS_BAD_COUNT] = "Неверное количество аргументов. \
Формат: image-transformer <source-image> <transformed-image> <angle>",
    [ARGS_PARSE_ANGLE_NAN] = "Угол должен быть числом",
    [ARGS_PARSE_ANGLE_UNSUPPORTED] = "Поддерживаются только углы: 0, 90, 180, 270, -90, -180, -270",
};

struct parsed_args parse_args(int32_t argc, char** argv) {
    if (argc != ARGC) {
        return (const struct parsed_args) { ARGS_BAD_COUNT, NULL, NULL, -1 };
    }
    const char* source_filename = argv[1];
    const char* transformed_filename = argv[2];
    const int32_t angle = atoi(argv[3]);
    if (angle == 0 && strcmp(argv[3], "0") != false) {
        return (const struct parsed_args) { ARGS_PARSE_ANGLE_NAN, NULL, NULL, -1 };
    }
    if (angle % 90 != 0) {
        return (const struct parsed_args) { ARGS_PARSE_ANGLE_UNSUPPORTED, NULL, NULL, -1 };
    }

    return (const struct parsed_args) { ARGS_PARSE_OK, source_filename, transformed_filename, angle };
}
