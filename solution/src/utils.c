#include "../include/utils.h"

/**
 * Поделить `a` на `b` с округлением вверх. Версия для `uint64_t`.
 *
 * @param a делимое
 * @param b делитель
 * @returns частное, округлённое вверх до целого
*/
uint64_t ceil_division_uint64(uint64_t a, uint64_t b) {
    return (uint64_t) ((a + b - 1) / b);
}

/**
 * Поделить `a` на `b` с округлением вверх.
 *
 * @param a делимое
 * @param b делитель
 * @returns частное, округлённое вверх до целого
*/
int32_t ceil_division(int32_t a, int32_t b) {
    return (int32_t) ((a + b - 1) / b);
}
