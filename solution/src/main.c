#include "../include/file.h"
#include "../include/args.h"
#include "../include/bmp.h"
#include "../include/rotate.h"


int32_t main(int32_t argc, char** argv) {
    const struct parsed_args args = parse_args(argc, argv);
    if (args.parse_status != ARGS_PARSE_OK) {
        printf("%s\n", ARGS_PARSING_ERRORS[args.parse_status]);
        return 1;
    }

    FILE* source_file = open_file(args.source_filename, READ_MODE);
    if (!source_file) {
        perror(FILE_OPEN_ERROR);
        return 1;
    }

    struct image img = { 0 };
    enum read_status r_status = from_bmp(source_file, &img);
    if (r_status != READ_OK) {
        printf("%s\n", READ_ERRORS[r_status]);
        return 1;
    }
    close_file(source_file);

    rotate_image(&img, args.angle);

    FILE* target_file = open_file(args.transformed_filename, WRITE_MODE);
    enum write_status w_status = to_bmp(target_file, &img);
    if (w_status != WRITE_OK) {
        printf("%s\n", WRITE_ERRORS[w_status]);
        return 1;
    }
    close_file(target_file);

    return 0;
}
